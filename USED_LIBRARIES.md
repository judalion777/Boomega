# Third party software used by the app

* [Used libraries](#used-libraries)
* [Used libraries/frameworks for testing](#used-librariesframeworks-for-testing)
* [Used Gradle plugins](#used-gradle-plugins)
* [Other acknowledgements](#other-acknowledgements)

## Used libraries

<table>
    <tr>
        <th>Software</th>
        <th>License</th>
    </tr>
    <tr>
        <td><a href="http://commons.apache.org/proper/commons-io/">Apache Commons IO</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="https://commons.apache.org/proper/commons-lang/">Apache Commons Lang</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="https://poi.apache.org">Apache POI</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="https://github.com/Typhon0/AnimateFX">AnimateFX</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="https://github.com/ben-manes/caffeine">Caffeine</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="https://github.com/controlsfx/controlsfx">ControlsFX</a></td>
        <td><a href="https://github.com/controlsfx/controlsfx/blob/master/license.txt">BSD 3-Clause License</a></td>
    </tr>
    <tr>
        <td><a href="https://bitbucket.org/Jerady/fontawesomefx/src/master/">FontAwesomeFX</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="http://github.com/dansoftowner/fxtaskbarprogressbar">FXTaskbarProgressbar</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="https://github.com/tom91136/GestureFX">GestureFX</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="https://github.com/google/gson">Gson</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="https://openjfx.io/">JavaFX</a></td>
        <td><a href="http://openjdk.java.net/legal/gplv2+ce.html">GPL v2 + Classpath</a></td>
    </tr>
    <tr>
        <td><a href="http://www.jasypt.org/index.html">Jasypt</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="https://github.com/JetBrains/java-annotations">Jetbrains annotations</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="https://kotlinlang.org/">Kotlin</a></td>
        <td><a href="https://github.com/JetBrains/kotlin/blob/master/license/LICENSE.txt">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="https://github.com/java-native-access/jna">JNA</a></td>
        <td><a href="https://github.com/java-native-access/jna/blob/master/LICENSE">Apache-2.0 OR LGPL-2.1</a></td>
    </tr>
    <tr>
        <td><a href="https://github.com/nico2sh/jHTML2Md">jHTML2Md</a></td>
        <td>-</td>
    </tr>
    <tr>
        <td><a href="https://jsoup.org/">jsoup</a></td>
        <td><a href="https://opensource.org/licenses/MIT">MIT License</a></td>
    </tr>
    <tr>
        <td><a href="https://github.com/Dansoftowner/jUserDirectories">jUserDirectories</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="https://github.com/Dansoftowner/jFileGoodies">jFileGoodies</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="https://github.com/Dansoftowner/jSystemThemeDetector">jSystemThemeDetector</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="http://www.jfoenix.com/">JFoenix</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="https://pixelduke.com/java-javafx-theme-jmetro/">JMetro</a></td>
        <td><a href="http://en.wikipedia.org/wiki/BSD_licenses#3-clause_license_.28.22Revised_BSD_License.22.2C_.22New_BSD_License.22.2C_or_.22Modified_BSD_License.22.29">New BSD license</a></td>
    </tr>
    <tr>
        <td><a href="http://www.sauronsoftware.it/projects/junique/">JUnique</a></td>
        <td>LGPL</td>
    </tr>
    <tr>
        <td><a href="http://logback.qos.ch/">Logback Project</a></td>
        <td><a href="http://logback.qos.ch/license.html">EPL v1.0 and LGPL 2.1</a></td>
    </tr>
    <tr>
        <td><a href="https://github.com/Dansoftowner/MarkdownEditorControlFX">MarkdownEditorControlFX</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="https://github.com/JPro-one/markdown-javafx-renderer">MDFX</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="https://github.com/nitrite/nitrite-java">Nitrite Database</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="https://github.com/0x4a616e/NSMenuFX">NSMenuFX</a></td>
        <td><a href="https://opensource.org/licenses/BSD-3-Clause">BSD-3-Clause License</a></td>
    </tr>
    <tr>
        <td><a href="https://github.com/oshi/oshi">OSHI</a></td>
        <td><a href="https://opensource.org/licenses/MIT">MIT License</a></td>
    </tr>
    <tr>
        <td><a href="https://square.github.io/okhttp/">OkHttp</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="https://github.com/FXMisc/RichTextFX">RichTextFX</a></td>
        <td><a href="https://github.com/FXMisc/RichTextFX/blob/master/LICENSE">BSD-2-Clause License</a></td>
    </tr>
    <tr>
        <td><a href="http://www.slf4j.org/">SLF4J API</a></td>
        <td><a href="https://opensource.org/licenses/MIT">MIT License</a></td>
    </tr>
    <tr>
        <td><a href="https://github.com/G00fY2/version-compare">Version Compare</a></td>
        <td><a href="https://github.com/G00fY2/version-compare/blob/master/LICENSE">Apache 2.0</a></td>
    </tr>
    <tr>
        <td><a href="https://github.com/dlsc-software-consulting-gmbh/WorkbenchFX">WorkbenchFX</a></td>
        <td><a href="https://www.apache.org/licenses/LICENSE-2.0">Apache 2.0</a></td>
    </tr>
</table>

## Used libraries/frameworks for testing

| Software | License |
| -------- | ------- |
| [JUnit 5](https://junit.org/junit5/) | [Eclipse Public License - v 2.0](https://github.com/junit-team/junit5/blob/main/LICENSE.md)
| [AssertJ](https://assertj.github.io/doc/) | [Apache-2.0 License](https://github.com/assertj/assertj-core/blob/main/LICENSE.txt)
| [Mockito](https://site.mockito.org/) | [MIT License](https://github.com/mockito/mockito/blob/release/3.x/LICENSE)

## Used Gradle plugins

| Software | License |
| -----    | ------- |
| [Shadow](https://github.com/johnrengelman/shadow) | [Apache 2.0](https://github.com/johnrengelman/shadow/blob/master/LICENSE)
| [gradle-versions-plugin](https://github.com/ben-manes/gradle-versions-plugin) | [Apache 2.0](https://github.com/ben-manes/gradle-versions-plugin/blob/master/LICENSE.txt)
| [jpackage-gradle-plugin](https://github.com/petr-panteleyev/jpackage-gradle-plugin) | [BSD-2-Clause License](https://github.com/petr-panteleyev/jpackage-gradle-plugin)

## Other acknowledgements

**Application icon:** [Freepik](https://www.flaticon.com/authors/freepik)

*Wallpapers:*

* [Wallpaper House](http://wallpaper-house.com)
* [PassMater](https://www.deviantart.com/passmater)
* [Freepik](https://www.freepik.com/)