/*
 * Boomega
 * Copyright (C)  2021  Daniel Gyoerffy
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.dansoftware.boomega.plugin;

import com.dansoftware.boomega.plugin.api.BoomegaPlugin;
import com.dansoftware.boomega.plugin.api.DisabledPlugin;
import com.dansoftware.boomega.util.ReflectionUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Responsible for loading and accessing the application plugins.
 *
 * @author Daniel Gyoerffy
 */
public class Plugins {

    private static final Logger logger = LoggerFactory.getLogger(Plugins.class);

    private static final Plugins INSTANCE = new Plugins();

    private final List<BoomegaPlugin> plugins = new ArrayList<>();
    private boolean loaded;

    private Plugins() {
    }

    /**
     * Loads and instantiates all the plugin classes found in the plugin-directory.
     */
    public synchronized void load() {
        if (!loaded) {
            plugins.addAll(
                    PluginClassLoader.getInstance().listAllClasses().stream()
                            .filter(this::isPluginUsable)
                            .peek(classRef -> logger.debug("Found plugin class: {}", classRef.getName()))
                            .map(ReflectionUtils::tryConstructObject)
                            .filter(Objects::nonNull)
                            .map(BoomegaPlugin.class::cast)
                            .peek(BoomegaPlugin::init)
                            .toList()
            );
            loaded = true;
        }
    }

    private boolean isPluginUsable(@NotNull Class<?> classRef) {
        return BoomegaPlugin.class.isAssignableFrom(classRef) &&
                !Modifier.isAbstract(classRef.getModifiers()) &&
                !classRef.isAnnotationPresent(DisabledPlugin.class);
    }

    /**
     * @return a read-only list with all the {@link BoomegaPlugin} objects
     */
    public List<BoomegaPlugin> getAll() {
        return Collections.unmodifiableList(plugins);
    }

    /**
     * Searches plugins that have the given location
     *
     * @param url the location
     * @return the list of {@link BoomegaPlugin} objects
     */
    public List<BoomegaPlugin> getPluginsOfFile(URL url) {
        return plugins.stream()
                .filter(it -> it.getClass().getProtectionDomain().getCodeSource().getLocation().equals(url))
                .toList();
    }

    /**
     * Searches plugins with the given type.
     *
     * @param classRef the class-reference of the type
     * @param <P>      the type, subtype of the {@link BoomegaPlugin}
     * @return the list of plugin objects
     */
    @SuppressWarnings("unchecked")
    public <P extends BoomegaPlugin> List<P> of(Class<P> classRef) {
        load();
        return plugins.stream()
                .filter(it -> classRef.isAssignableFrom(it.getClass()))
                .map(it -> (P) it)
                .toList();
    }

    /**
     * @return the count of the loaded plugin files
     */
    public int pluginFileCount() {
        return PluginClassLoader.getInstance().getReadPluginsCount();
    }

    /**
     * @return the count of the constructed plugin objects
     */
    public int pluginObjectCount() {
        return plugins.size();
    }

    public static Plugins getInstance() {
        return INSTANCE;
    }
}
