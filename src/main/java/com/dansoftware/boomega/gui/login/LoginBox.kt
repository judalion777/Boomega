/*
 * Boomega
 * Copyright (C)  2021  Daniel Gyoerffy
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.dansoftware.boomega.gui.login

import com.dansoftware.boomega.config.Preferences
import com.dansoftware.boomega.config.logindata.LoginData
import com.dansoftware.boomega.db.Credentials
import com.dansoftware.boomega.db.DatabaseMeta
import com.dansoftware.boomega.gui.api.Context
import com.dansoftware.boomega.gui.entry.DatabaseTracker
import com.dansoftware.boomega.gui.util.icon
import com.dansoftware.boomega.gui.util.refresh
import com.dansoftware.boomega.i18n.i18n
import javafx.beans.property.*
import javafx.beans.value.ObservableStringValue
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Group
import javafx.scene.control.*
import javafx.scene.image.ImageView
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class LoginBox(private val controller: Controller) : VBox(10.0) {

    private val itemSelected: BooleanProperty = SimpleBooleanProperty()
    private val usernameInput: StringProperty = SimpleStringProperty()
    private val passwordInput: StringProperty = SimpleStringProperty()
    private val remember: BooleanProperty = SimpleBooleanProperty()
    private val databaseChooser: ObjectProperty<ComboBox<DatabaseMeta>> = SimpleObjectProperty()

    var selectedItem: DatabaseMeta?
        get() = databaseChooser.get().selectionModel.selectedItem
        set(value) {
            select(value)
        }

    init {
        this.styleClass.add("login-box")
        this.maxWidth = 650.0
        this.prefWidth = 550.0
        this.buildUI()
        controller.loginBox = this
    }

    fun titleProperty(): ObservableStringValue = databaseChooser.get().selectionModel.selectedItemProperty().asString()

    fun refresh() {
        databaseChooser.get().refresh()
    }

    fun addItem(item: DatabaseMeta) {
        databaseChooser.get().items.add(item)
    }

    fun removeItem(item: DatabaseMeta) {
        databaseChooser.get().items.remove(item)
    }

    fun select(databaseMeta: DatabaseMeta?) {
        databaseChooser.get().selectionModel.select(databaseMeta)
    }

    fun addSelectedItemListener(listener: (DatabaseMeta?) -> Unit) {
        databaseChooser.get().selectionModel.selectedItemProperty().addListener { _, _, it -> listener(it) }
    }

    fun fillForm(loginData: LoginData) {
        databaseChooser.get().let { databaseChooser ->
            databaseChooser.items.addAll(loginData.savedDatabases)
            loginData.selectedDatabase?.let(databaseChooser.selectionModel::select)
            loginData.autoLoginDatabase?.let {
                remember.set(true)
                loginData.autoLoginCredentials?.run {
                    usernameInput.set(username)
                    passwordInput.set(password)
                }
            }
        }
    }

    private fun buildUI() {
        children.add(buildHeader())
        children.add(buildDatabaseChooserArea())
        children.add(buildForm())
        children.add(Separator())
        children.add(buildDataSourceButton())
    }

    private fun buildHeader() = StackPane().apply {
        styleClass.add("header")
        padding = Insets(20.0)
        HBox(10.0).run {
            children.add(ImageView().apply { styleClass.add("logo") })
            children.add(StackPane(Label(System.getProperty("app.name"))))
            Group(this)
        }.let(children::add)
    }

    private fun buildDatabaseChooserArea() = HBox(5.0).apply {
        children.add(buildComboBox())
        children.add(buildFileChooserButton())
        children.add(buildDatabaseManagerButton())
        setMargin(this, Insets(0.0, 20.0, 0.0, 20.0))
    }

    private fun buildComboBox() = ComboBox<DatabaseMeta>().apply {
        databaseChooser.set(this)
        minHeight = 35.0
        minWidth = 355.0
        maxWidth = Double.MAX_VALUE
        promptText = i18n("login.source.combo.promt")
        buttonCell = ComboBoxButtonCell(controller.databaseTracker)
        setCellFactory { DatabaseChooserItem(controller.databaseTracker) }
        itemSelected.bind(selectionModel.selectedItemProperty().isNotNull)
        HBox.setHgrow(this, Priority.ALWAYS)
    }

    private fun buildFileChooserButton() = Button().apply {
        tooltip = Tooltip(i18n("login.source.open"))
        graphic = icon("folder-open-icon")
        contentDisplay = ContentDisplay.GRAPHIC_ONLY
        minHeight = 35.0
        minWidth = 40.0
        setOnAction {
            controller.openFile()
        }
    }

    private fun buildDatabaseManagerButton() = Button().apply {
        tooltip = Tooltip(i18n("login.db.manager.open"))
        graphic = icon("database-icon")
        contentDisplay = ContentDisplay.GRAPHIC_ONLY
        minHeight = 35.0
        minWidth = 40.0
        setOnAction {
            controller.openDatabaseManager()
        }
    }

    private fun buildDataSourceButton() = Button().apply {
        minHeight = 35.0
        styleClass.add("source-adder")
        text = i18n("login.add.source")
        maxWidth = Double.MAX_VALUE
        graphic = icon("database-plus-icon")
        setOnAction {
            controller.openDatabaseCreator()
        }
    }

    private fun buildForm() = VBox(10.0).apply {
        children.add(Separator())
        children.add(buildUsernameInput())
        children.add(buildPasswordInput())
        children.add(buildCheckBox())
        children.add(buildLoginButton())
        managedProperty().bind(itemSelected)
        visibleProperty().bind(itemSelected)
        setMargin(this, Insets(0.0, 20.0, 20.0, 20.0))
    }

    private fun buildUsernameInput() = TextField().apply {
        minHeight = 35.0
        prefColumnCount = 10
        promptText = i18n("credentials.username")
        usernameInput.bindBidirectional(textProperty())
        textProperty().bindBidirectional(usernameInput)
    }

    private fun buildPasswordInput() = PasswordField().apply {
        minHeight = 35.0
        prefColumnCount = 10
        promptText = i18n("credentials.password")
        passwordInput.bindBidirectional(textProperty())
        textProperty().bindBidirectional(passwordInput)
    }

    private fun buildCheckBox() = CheckBox().apply {
        alignment = Pos.CENTER_RIGHT
        text = i18n("login.form.remember")
        remember.bindBidirectional(selectedProperty())
        selectedProperty().bindBidirectional(remember)
    }

    private fun buildLoginButton() = Button().apply {
        minHeight = 35.0
        maxWidth = Double.MAX_VALUE
        text = i18n("login.form.login")
        isDefaultButton = true
        setOnAction {
            databaseChooser.get().selectionModel.selectedItem?.let { dbMeta ->
                controller.login(
                    dbMeta, Credentials(
                        usernameInput.get()?.trim() ?: "",
                        passwordInput.get()?.trim() ?: ""
                    ), remember.get()
                )
            }
        }
    }

    private open class DatabaseChooserItem(
        private val databaseTracker: DatabaseTracker
    ) : ListCell<DatabaseMeta?>() {

        companion object {
            private const val NOT_EXISTS_CLASS = "state-indicator-file-not-exists"
            private const val USED_CLASS = "state-indicator-used"
        }

        init {
            maxWidth = 650.0
        }

        override fun updateItem(item: DatabaseMeta?, empty: Boolean) {
            super.updateItem(item, empty)
            when {
                item == null || empty -> {
                    text = null
                    graphic = null
                }
                else -> {
                    text = item.toString()
                    val dbFile = item.file!!
                    when {
                        dbFile.exists().not() || dbFile.isDirectory -> {
                            tooltip = Tooltip(i18n("file.not.exists"))
                            graphic = icon("warning-icon").apply { styleClass.add(NOT_EXISTS_CLASS) }
                        }
                        databaseTracker.isDatabaseUsed(item) -> {
                            tooltip = Tooltip(i18n("database.currently.used"))
                            graphic = icon("play-icon").apply { styleClass.add(USED_CLASS) }
                        }
                        else -> {
                            graphic = null
                            tooltip = null
                        }
                    }
                }
            }
        }
    }

    private class ComboBoxButtonCell(databaseTracker: DatabaseTracker) : DatabaseChooserItem(databaseTracker) {
        override fun updateItem(item: DatabaseMeta?, empty: Boolean) {
            super.updateItem(item, empty)
            if (item === null) {
                text = i18n("login.source.combo.promt")
            }
        }
    }

    interface Controller {
        var loginBox: LoginBox?

        val context: Context
        val preferences: Preferences
        val databaseTracker: DatabaseTracker
        val loginData: LoginData

        fun openDatabaseManager()
        fun openFile()
        fun openDatabaseCreator()
        fun login(databaseMeta: DatabaseMeta, credentials: Credentials, remember: Boolean)
    }

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(LoginBox::class.java)
    }
}
